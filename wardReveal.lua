--[[
	Breaking Bad Ward Scanner
	v1.2
	
	Displays enemy wards / shaco boxes / teemo mushrooms / maokai's saplings / caitlyn and nidalee traps on mini-map and in-game.
	Written by Weee
	
	DON'T FORGET TO CHOOSE YOUR RESOLUTION!
]]

--[[ 		Config		]]
pingWardsHotkey = 84		-- Hotkey for pinging all present wards/boxes/shrooms/traps on minimap. Only you will see it. Default: "T"

--[[		Choosing Resolution:	]]
--ResolutionOffsets = {1627,897,1907,1182}	--1920x1200
--ResolutionOffsets = {1627,777,1907,1062}	--1920x1080
--ResolutionOffsets = {1387,747,1667,1032}	--1680x1050
--ResolutionOffsets = {1322,736,1587,1007}	--1600x1024
--ResolutionOffsets = {1322,612,1587,883}	--1600x900
--ResolutionOffsets = {1190,641,1428,883}	--1440x900
ResolutionOffsets = {1128,520,1355,751}	--1366x768
--ResolutionOffsets = {1124,520,1349,751}	--1360x768
--ResolutionOffsets = {1057,793,1269,1011}	--1280x1024
--ResolutionOffsets = {1057,729,1269,947}	--1280x960
--ResolutionOffsets = {1057,569,1269,787}	--1280x800
--ResolutionOffsets = {1057,535,1269,753}	--1280x768
--ResolutionOffsets = {1057,487,1269,705}	--1280x720

--[[ 		Globals		]]
wards = {}

--[[ 		Code		]]
function Scanner()
	wardCount = 1
	for i = 1, GetUnitCount(), 1 do
		object = GetUnit(i)
		if object ~= nil and object.team == TEAM_ENEMY then
			if object.name == "SightWard" or object.name =="WriggleLantern" then
				wardObject = {}
				wardObject.object = object
				wardObject.color = {0,1,0.4,1}
				wardObject.overlayMark = "w"
				wardObject.inGameMarkType = 1
				wards[ wardCount ] = wardObject
				wardCount = wardCount + 1
			end
			if object.name == "VisionWard" then
				wardObject = {}
				wardObject.object = object
				wardObject.color = {1,0.3,1,1}
				wardObject.overlayMark = "w"
				wardObject.inGameMarkType = 1
				wards[ wardCount ] = wardObject
				wardCount = wardCount + 1
			end
			if object.charName == "ShacoBox" or object.charName == "TeemoMushroom" or object.charName == "MaokaiSproutling" then
				wardObject = {}
				wardObject.object = object
				wardObject.color = {1,0.3,0.3,1}
				wardObject.overlayMark = "o"
				wardObject.inGameMarkType = 2
				wards[ wardCount ] = wardObject
				wardCount = wardCount + 1
			end
			if object.charName == "CaitlynTrap" or object.charName == "Nidalee_Spear" then
				wardObject = {}
				wardObject.object = object
				wardObject.color = {0.9,0.9,1,1}
				wardObject.overlayMark = "o"
				wardObject.inGameMarkType = 3
				wards[ wardCount ] = wardObject
				wardCount = wardCount + 1
			end
		end
	end
end

function WardDrawer()
	for i,ward in ipairs(wards) do
		if (ward.object ~= nil and ward.object.health > 0 and ward.object.team == TEAM_ENEMY)
		and (ward.object.charName == "SightWard" or ward.object.charName == "WriggleLantern" or ward.object.charName == "VisionWard"
		or ward.object.charName == "ShacoBox" or ward.object.charName == "TeemoMushroom" or ward.object.charName == "MaokaiSproutling"
		or ward.object.charName == "CaitlynTrap" or ward.object.charName == "Nidalee_Spear") then
			local mapx, mapz
			if ward.inGameMarkType == 1 then
				DrawCircle(100,ward.object.x,ward.object.y,ward.object.z)
			end
			if ward.inGameMarkType == 2 then
				DrawCircle(90,ward.object.x,ward.object.y,ward.object.z)
				DrawCircle(60,ward.object.x,ward.object.y,ward.object.z)
			end
			if ward.inGameMarkType == 3 then
				DrawCircle(60,ward.object.x,ward.object.y,ward.object.z)
				DrawCircle(40,ward.object.x,ward.object.y,ward.object.z)
			end
			mapx = ResolutionOffsets[1] + premapx * ward.object.x
			mapz = ResolutionOffsets[4] - premapz * ward.object.z
			DrawText(ward.overlayMark,mapx,mapz,ward.color[1],ward.color[2],ward.color[3],ward.color[4])
		end
	end
end

function PingWards( msg, keycode )
    if msg == KEY_DOWN or keycode ~= pingWardsHotkey then
       return
    end
	for i,ward in ipairs(wards) do
		if ward.object ~= nil and (ward.object.maxMana == 180 or ward.object.maxMana == 60 or ward.object.maxMana == 600 or ward.object.maxMana == 25 or ward.object.maxMana == 500) then
			if ward.inGameMarkType == 1 then
				PingSignal(PING_NORMAL,ward.object.x,ward.object.z)
			else
				PingSignal(PING_FALLBACK,ward.object.x,ward.object.z)
			end
		end
	end
end

function Load()
	premapx = ( ResolutionOffsets[3] - ResolutionOffsets[1] ) / 14007
	premapz = ( ResolutionOffsets[4] - ResolutionOffsets[2] ) / 14439
	script.timerCallback = {"Scanner",250}
	script.drawCallback = "WardDrawer"
	script.keyCallback = "PingWards"
	PrintChat(" >> Breaking Bad Ward Scanner script loaded!")
end