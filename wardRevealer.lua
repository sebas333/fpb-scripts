--[[
	Reveals enemy wards
	v1.0
	Written by Zynox
]]

--[[ 		Config		]]
pingWards = true --Ping new wards from enemies
warnChat = true --Print a warn message to chat (only visible to yourself)
wardRadius = 50 --Circle radius around wards

--[[ 		Globals		]]
wards = {}

--[[ 		Code		]]
function Add(object)
	if object.name ~= "SightWard" and object.name ~= "VisionWard" then 
		return 
	end
	wardObject = {}
	wardObject.tick = GetTick()
	wardObject.object = object
	wardObject.notify = true
	table.insert(wards,wardObject)	
end

function Delete(object)
	if object.name ~= "Data\Particles\Sightward.troy" and object.name ~= "Data\Particles\VisionWard.troy" then 
		return 
	end
	for i,ward in ipairs(wards) do
		if ward.object:GetDistanceTo(object) < 50 then
			table.remove(wards,i)
			return
		end
	end
end

function Drawer()
	for i,ward in ipairs(wards) do
		if ward.object.team == TEAM_ENEMY then
			DrawCircle(wardRadius,ward.object.x,ward.object.y,ward.object.z)
		end
	end
end

function Timer( tick )
	for i,ward in ipairs(wards) do
		if ward.object.team == player.team or (tick-ward.tick) >= (3*60*1000) then
			table.remove(wards,i)
		else
			if ward.notify == true then
				ward.notify = false
				if pingWards == true then
					PingSignal(PING_NORMAL,ward.object.x,ward.object.z)
				end
				if warnChat == true then
					PrintChat(string.format(" >> Enemy set a %s",ward.object.name))
				end
			end
		end
	end
end

function Load()
	script.createObjectCallback = "Add"
	script.deleteObjectCallback = "Delete"
	script.drawCallback = "Drawer"
	script.timerCallback = {"Timer",500}
	PrintChat(" >> Ward revealer script loaded!")
end