--[[
	Displays the range of enemy auto attacks
	v1.1
	Written by Zynox
]]

--[[ 		Globals		]]
enemies = {}

--[[ 		Code		]]
function Timer( tick )
	enemies = {}
	local count = GetPlayerCount()
	for i = 1, count, 1 do
		local object = GetPlayer(i) --(index from 1 to count)
		if object.team == TEAM_ENEMY and object.dead == false and object.visible == true then
			table.insert(enemies,object)	
		end
	end
end

function Drawer()
	for i,object in ipairs(enemies) do
		DrawCircle(object.range, object.x, object.y, object.z ) --(range, x, y, z)
	end
end


function Load()
	script.drawCallback = "Drawer"
	script.timerCallback = {"Timer",350}
	PrintChat(" >> Attack range script loaded!")
end