--[[
	AlistarWQ
	v1.2
	
	written by Weee
	tracking W cooldown idea by barasia283
	
	Makes a WQ combo on Alistar while holding spacebar and using headbutt on your target (works perfectly with smartcast, didn't test it without smartcast, but should work fine).
	If you want to use headbutt without pulverize - then just don't hold spacebar and use W like always.
]]

Hotkey = 32 -- spacebar
HeadButtTick = nil
IgnoreWQ = true

function HeadbuttScan()
	if IgnoreWQ == true then return end
	if player:CanUseSpell(SPELL_SLOT_2) == STATE_COOLDOWN then
		if HeadButtTick == nil then
			HeadButtTick = GetTick()
		else
			if GetTick() - HeadButtTick >= 500 then
				return
			end
		end
		if player:CanUseSpell(SPELL_SLOT_1) == STATE_READY then
			for j = 1, PlayerCount, 1 do
				local playerobject = GetPlayer(j)
				if playerobject ~= nil and playerobject.team == TEAM_ENEMY and player:GetDistanceTo(playerobject) <= 365 and not playerobject.dead then
					player:UseSpell(SPELL_SLOT_1)
					return
				end
			end
		end
	else
		HeadButtTick = nil
	end
end

function Keypress(msg, keycode)
	if keycode ~= Hotkey then return end
	if msg == KEY_DOWN and keycode == Hotkey then
		IgnoreWQ = false
		return
	end
	if msg == KEY_UP and keycode == Hotkey then
		IgnoreWQ = true
	end
end

function Load()
	if player.charName ~= "Alistar" then 
		script:Unload()
		return
	end
	PlayerCount = GetPlayerCount()
	script.drawCallback = "HeadbuttScan"
	script.keyCallback = "Keypress"
	PrintChat(" >> AlistarWQ loaded!")
end