--[[
	Auto ignite if possible v2.0
	ReWritted by Burn-MBIstari, based on Zynox smite script
]]

--[[		Config		]]
igniteSlot = SPELL_SUMMONER_2 -- set to SPELL_SUMMONER_1 or SPELL_SUMMONER_2 or nil to unload script 
printChatKey = 112 -- key for enable or disable auto ignite (default: F1) (you can use a=65, y=89, j=74, etc.)  
printChat = true
range = 600 --range of ignite (~600)                                   


--[[ 		Globals		]]
sleepTick = 0


--[[ 		Code		]]
function Timer( tick )
	if sleepTick >= tick then
		return
	end
	local igniteDamage = 50 + 20 * player.level	
	local count = GetUnitCount()
	for i = 1, count, 1 do
		local object = GetPlayer(i)
		if object ~= nil and object.team == TEAM_ENEMY and object.dead == false and player:GetDistanceTo(object) < range and object.health < igniteDamage then
				if printChat == true then
				player:UseSpell( igniteSlot, object )
				Sleep(200)
				return
				end
		end
	end
end


function Hotkey( msg, keycode )
	if msg == KEY_DOWN then
	   return
	end

	if keycode == printChatKey then
		if printChat == false then
			printChat = true
			PrintChat("<font color='#7CFC00'> >>AutoIgnite enabled!</font>")
		else
			PrintChat("<font color='#FF4500'> >>Autoignite disabled!</font>")
			printChat = false
		end
	end
end


function Sleep( ms )
	sleepTick = GetTick() + ms
end


function Load()
	if igniteSlot == nil then
		script:Unload()
		return
	end
	script.keyCallback = "Hotkey"
	script.timerCallback = {name = "Timer", interval = 200}
    PrintChat(" >> Auto Ignite script loaded!")
end