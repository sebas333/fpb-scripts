--[[
	Pick a Card
	v1.2 mod 3
	Written by Sumizome
	(Updated by Zynox)
	(Modification by Weee)
]]

--[[         Config        ]]
method = 1			-- 0 - old method (swaping cards with 1 hotkey); 1 - new method (3 hotkeys for each card. by pressing one of them script will automatically pick right card)
Hotkey = 74			-- J
HotkeyRed = 90		-- Z
HotkeyYellow = 67	-- C
HotkeyBlue = 86 	-- V

--[[         Globals        ]]
cards = {"Card_Red", "Card_Yellow", "Card_Blue"}
card = 1

--[[         Code        ]]
function Keypress(msg, keycode)
	if msg == KEY_UP then return end
	if method == 0 and keycode == Hotkey then
		card = card + 1
		if card > 3 then card = 1 end
		if card == 1 then PrintChat(string.format(">> Selected <font color='#FF0000'>%s Card</font>", cards[card])) end
		if card == 2 then PrintChat(string.format(">> Selected <font color='#FFFF33'>%s Card</font>", cards[card])) end
		if card == 3 then PrintChat(string.format(">> Selected <font color='#3399FF'>%s Card</font>", cards[card])) end
	end
	if method == 1 and player:CanUseSpell(SPELL_SLOT_2) == STATE_READY and (keycode == HotkeyRed or keycode == HotkeyYellow or keycode == HotkeyBlue) then
		if keycode == HotkeyRed then card = 1 end
		if keycode == HotkeyYellow then card = 2 end
		if keycode == HotkeyBlue then card = 3 end
		player:UseSpell(SPELL_SLOT_2)
	end
end

function DrawerCard()
	for i = 1, GetUnitCount(), 1 do
		local object = GetUnit(i)
		if object ~= nil and player:GetDistanceTo(object) == 0 and object.visible == true and string.find(object.name, cards[card]) then 
			player:UseSpell(SPELL_SLOT_2)
		end
	end
end

function Load()
	if player.charName ~= "TwistedFate" then 
		script:Unload()
		return
	end
	script.drawCallback = "DrawerCard"
	script.keyCallback = "Keypress"
	PrintChat(" >> Pick a Card loaded!")
end