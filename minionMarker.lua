--[[
	Displays a circle for all minions, which can be killed with an auto attack
	v1.1
	Written by Zynox
]]
--[[ 		Config		]]
range = 1200 --range of minions near player
circleRadius = 70 --radius of circles on minions
thickness = 4 --thickness of circle

--[[ 		Globals		]]
units = {}

--[[ 		Code		]]
function Timer( tick )
	units = {}
	--Get units to draw in our drawer func
	local count = GetUnitCount()
	for i = 1, count, 1 do
		local object = GetUnit(i) --(index from 1 to count)
		if object ~= nil and object.team == TEAM_ENEMY and string.find(object.name,"Minion_") == 1 and object.dead == false then
			if player:GetDistanceTo(object) < range and object.health <= player:CalculateDamage(object) then
				table.insert( units, object )
			end
		end
	end
end

--No unnecessary code in the drawer..
function Drawer()
	for i,object in ipairs(units) do
		if object.dead == false then
			for j = 0, thickness, 1 do
				DrawCircle(circleRadius + j*2,object.x,object.y,object.z)
			end
		end
	end
end


function Load()
	-- no invalid/negative values!
	thickness = math.max(thickness,1)
	
	script.drawCallback = "Drawer"
	script.timerCallback = {name = "Timer", interval = 250} -- = {"Timer",250} would work too!
	PrintChat(" >> Minion marker script loaded!")
end