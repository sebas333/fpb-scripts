function printAllFunctions(obj, print_func)
	if(obj==nil) then return end
	print_func("Members:")

	for k,v in pairs(obj) do
		print_func(">> "..k)
	end
end

-- usage printAllFunctions(player,PrintChat)