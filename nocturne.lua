scriptActive = false
HK=113

function altDoFile(name)
        dofile(debug.getinfo(1).source:sub(debug.getinfo(1).source:find(".*\\")):sub(2)..name)end

altDoFile("target_selector.lua")
altDoFile("vector.lua")
altDoFile("linear_prediction.lua")
altDoFile("combos_lib.lua")

function simple_test(combo)
    for k,v in pairs(combo) do
        if (v.type~=SPELL_TYPE_USERFUNC and player:CanUseSpell(v.slot)~=STATE_READY) then
            return false
        end
    end
    return true
end

function checkRange(target)
        return  player:GetDistanceTo(target)<425 and player:CanUseSpell(SPELL_SLOT_3)==STATE_READY

end

function waitForCd( obj )
    return player:CanUseSpell(SPELL_SLOT_4)==STATE_COOLDOWN
end
Q = Spell:new(SPELL_TYPE_SKILLSHOT,SPELL_SLOT_1)
W = Spell:new(SPELL_TYPE_SELF,SPELL_SLOT_2)
E = Spell:new(SPELL_TYPE_NORMAL,SPELL_SLOT_3)
R = Spell:new(SPELL_TYPE_NORMAL,SPELL_SLOT_4)
D = Spell:new(SPELL_TYPE_USERFUNC,waitForCd)


--create new ComboSelector
cr = ComboSelector:new()
--setup combos
cr:add_combo(Combo:new({R,R,D,W,Q},3500,simple_test))
cr:add_combo(Combo:new({R,R,D,Q},3500,simple_test))
cr:add_combo(Combo:new({Q},1200,simple_test))
cr:add_conditional_spell(E,checkRange)
--create new ComboSelector
ct = ComboSelector:new()
--setup combos
ct:add_combo(Combo:new({R,R,D,W,Q},2750,simple_test))
ct:add_combo(Combo:new({R,R,D,Q},2750,simple_test))
ct:add_combo(Combo:new({Q},1200,simple_test))
ct:add_conditional_spell(E,checkRange)
--create new ComboSelector
cy = ComboSelector:new()
--setup combos
cy:add_combo(Combo:new({R,R,D,W,Q},2000,simple_test))
cy:add_combo(Combo:new({R,R,D,Q},2000,simple_test))
cy:add_combo(Combo:new({Q},1200,simple_test))
cy:add_conditional_spell(E,checkRange)

--get most strong awayilable combo

lp = LinearPrediction:new(1200,1.1)
ts = TargetSelector:new(TARGET_LOW_HP,1200)


current_combo = nil
function Timer( tick )
    if player.dead == true then return end
    lp:tick()
    if(scriptActive) then
        if(current_combo==nil) then
                ts:tick()
        else
            ts.range=current_combo.range
            if(ts.target) then
                current_combo:runNextSpell(ts.target,lp:getPredictionFor(ts.target.name))
            	        player:Attack(ts.target)
            end
            if(current_combo.finished) then
            	if (player.level < 11) then
                current_combo = cy:get_combo()
				
				elseif (player.level >= 11 and player.level <=15) then
                current_combo = ct:get_combo()
                else
                current_combo = cr:get_combo()
				end
            end
        end
    else
                     	if (player.level < 11) then
                current_combo = cy:get_combo()
				elseif (player.level >= 11 and player.level <=15) then
                current_combo = ct:get_combo()
                else
                current_combo = cr:get_combo()
				end
         if(current_combo)then
                    ts.range=current_combo.range
                end
       ts:tick() 
    end
end

function Drawer()
    if player.dead == true then return end
    if(current_combo) then
        DrawCircle(current_combo.range,player.x, player.y, player.z)
        DrawText(""..current_combo:to_s(),WINDOW_W-150,WINDOW_H/2,1,0,0,1)
    end
    if ts.target ~= nil then
        DrawCircle(100,ts.target.x, ts.target.y, ts.target.z)
         p = lp:getPredictionFor(ts.target.name)
        if p then
            DrawCircle(30,p.x, ts.target.y, p.z)
        end
    end
end
function Hotkey(msg,key)
    if msg == KEY_DOWN then
        --key down
        if key == HK then
            scriptActive = true
        end
    else
        if key == HK then
            scriptActive = false
            if(current_combo) then
                current_combo.finished=false
                current_combo.index=1
                current_combo = nil
            end
        end
    end
end

function Load()
    if player.charName ~= "Nocturne" then 
        script:Unload()
        return
    end
    script.keyCallback = "Hotkey"
    script.timerCallback = { name = "Timer", interval = 100 }--ms 
    script.drawCallback = "Drawer"
    PrintChat(" Nocturne helper 0.1 by h0nda (Sebas Edit)")
    

end
