--[[
	Displays the range of towers
	v1.3
	Written by Zynox
]]
--[[ 		Config		]]
ownTurrents = false
enemyTurrets = true
spawnTurrets = true

--[[ 		Globals		]]
towers = {}

--[[ 		Code		]]
function FetchTowers()
	towers = {}
	local count = GetUnitCount()
	for i = 1, count, 1 do
		local object = GetUnit(i)
		if object ~= nil and string.find(object.name,"Turret_") ~= nil and string.find(object.name,"_A") == nil and object.health > 0 then
				if (enemyTurrets == true and object.team == TEAM_ENEMY) 
					or (ownTurrents == true and player.team == object.team) 
					or (spawnTurrets == true and object.team == TEAM_NONE) then 
						table.insert(towers,object)		
				end
		end
	end
end

function Drawer()
	for i,tower in ipairs(towers) do
		if tower.health > 0 then
			DrawCircle(1000,tower.x,tower.y,tower.z)
		end
	end
end

function Load()
	FetchTowers()
	script.drawCallback = "Drawer"
	PrintChat(" >> Tower range script loaded!")
end