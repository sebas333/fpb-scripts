Vector = {}
Vector.__index = Vector


function Vector.__add(a, b)
  if type(a) == "number" then
    return Vector.new(b.x + a, b.z + a)
  elseif type(b) == "number" then
    return Vector.new(a.x + b, a.z + b)
  else
    return Vector.new(a.x + b.x, a.z + b.z)
  end
end

function Vector.__sub(a, b)
   assert(type(a) ~= "number"," cant sub vector from number")
  if type(b) == "number" then
    return Vector.new(a.x - b, a.z - b)
  else
    return Vector.new(a.x - b.x, a.z - b.z)
  end
end

function Vector:__unm()
  return self*(-1)
end

function Vector.__mul(a, b)
  if type(a) == "number" then
    return Vector.new(b.x * a, b.z * a)
  elseif type(b) == "number" then
    return Vector.new(a.x * b, a.z * b)
  else
    return Vector.new(a.x * b.x, a.z * b.z)
  end
end

function Vector.__div(a, b)
  if type(a) == "number" then
    return Vector.new(b.x / a, b.z / a)
  elseif type(b) == "number" then
    return Vector.new(a.x / b, a.z / b)
  else
    return Vector.new(a.x / b.x, a.z / b.z)
  end
end

function Vector.__eq(a, b)
  return a.x == b.x and a.z == b.z
end

function Vector.__lt(a, b)
  return a.x < b.x or (a.x == b.x and a.z < b.z)
end

function Vector.__le(a, b)
  return a.x <= b.x and a.z <= b.z
end

function Vector.__tostring(a)
  return "(" .. a.x .. ", " .. a.z .. ")"
end

function Vector.new(x, z)
  return setmetatable({ x = x or 0, z = z or 0 }, Vector)
end

function Vector.distance(a, b)
  return (b - a):len()
end

function Vector:clone()
  return Vector.new(self.x, self.z)
end

function Vector:unpack()
  return self.x, self.z
end

function Vector:len()
  return math.sqrt(self.x * self.x + self.z * self.z)
end

function Vector:lenSq()
  return self.x * self.x + self.z * self.z
end

function Vector:normalize()
  local len = self:len()
  self.x = self.x / len
  self.z = self.z / len
  return self
end

function Vector:normalized()
  return self / self:len()
end

function Vector:rotate(phi)
  local c = math.cos(phi)
  local s = math.sin(phi)
  self.x = c * self.x - s * self.z
  self.z = s * self.x + c * self.z
  return self
end

function Vector:rotated(phi)
  return self:clone():rotate(phi)
end

function Vector:perpendicular()
  return Vector.new(-self.z, self.x)
end

function Vector:projectOn(other)
  return (self * other) * other / other:lenSq()
end

function Vector:cross(other)
  return self.x * other.z - self.z * other.x
end

setmetatable(Vector, { __call = function(_, ...) return Vector.new(...) end })