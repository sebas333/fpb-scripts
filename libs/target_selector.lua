TARGET_LOW_HP = 1
TARGET_NEAR = 2
TARGET_FAR = 3
DAMAGE_MAGIC = 1
DAMAGE_PHYSICAL = 2

TargetSelector = {}

function TargetSelector:new(mode, range, damageType)
    local object = { mode = mode, range = range, damageType = damageType or DAMAGE_MAGIC, target = nil }
    setmetatable(object, { __index = TargetSelector })  
    return object
end

function CalculateMagicDmg(target, spellDamage) --TODO replace this with working target:CalculateMagicDamage() in next update
	local magicArmor = target.magicArmor - player.magicPen
	if magicArmor > 0 then
		magicArmor = magicArmor * (1-(player.magicPenPercent/100))
	else
		magicArmor = 0
	end
	return spellDamage * (100/(100+magicArmor))
end

function TargetSelector:check_if_target(obj)
    return obj ~= nil and obj.team == TEAM_ENEMY and obj.visible and not obj.dead
end

function TargetSelector:low_hp()
    local tmp
    local count = GetPlayerCount()
    for i = 1, count, 1 do
        local object = GetPlayer(i) 
		if self:check_if_target(object) and player:GetDistanceTo(object) <= self.range
		and
		 (tmp == nil or
		  ((self.damageType == DAMAGE_MAGIC and tmp.health * (100/CalculateMagicDmg(tmp,100)) > object.health * (100/CalculateMagicDmg(object,100)))
		   or
		  (self.damageType == DAMAGE_PHYSICAL and tmp.health * (player.totalDamage/player:CalculateDamage(tmp)) > object.health * (player.totalDamage/player:CalculateDamage(object))))
		 ) then
			tmp = object
		end
    end
    self.target = tmp
end

function TargetSelector:near()
	local nc  = nil
    local smallest_dist = self.range+1
    local count = GetPlayerCount()
    for i = 1, count, 1 do
        local object = GetPlayer(i) 
        if  self:check_if_target(object)  then
            local d =  player:GetDistanceTo(object)
            if d<=self.range and d<smallest_dist then
                nc = object
                smallest_dist = d
            end
        end
    end
    self.target = nc
end

function TargetSelector:far()
	local fc  = nil
    local biggest_dist = -1
    local count = GetPlayerCount()
    for i = 1, count, 1 do
        local object = GetPlayer(i) 
        if  self:check_if_target(object)  then
            local d =  player:GetDistanceTo(object)
            if d<=self.range and d>biggest_dist then
                fc = object
                biggest_dist = d
            end
        end
    end
    self.target = fc
end

function TargetSelector:tick(time)
	if self.mode == TARGET_LOW_HP then
		self:low_hp()
	elseif self.mode == TARGET_FAR then
		self:far()
	else
		self:near()
	end
end