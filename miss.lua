--[[
	Auto miss feature for enemies
	v1.2
	Written by Zynox
]]

--[[		Config		]]
sendChat = false -- initial state of send chat feature
printChat = true -- initial state of print chat feature

sendChatKey = 118 -- key for toggling send chat feature (F8=118)
printChatKey = nil -- key for toggling print chat feature                                       

missTime = 5000 -- time in ms between sending miss to chat (antispam)
missRange = 2000 -- range between player and miss object

--[[ 		Globals		]]
playerTimer = {}
lastMissTick = 0

--[[ 		Code		]]
function Timer( tick )
	local count = GetPlayerCount()
	for i = 1, count, 1 do
		local object = GetPlayer(i)
		if object.team == TEAM_ENEMY and object.dead == false then
			if object.visible == true then
				if printChat == true and playerTimer[i] == nil then
					PrintChat(string.format("<font color='#7CFC00'> >> re %s</font>", object.charName))
					PingSignal(PING_FALLBACK,object.x,object.z)
				end
				playerTimer[ i ] = tick
			else
				if playerTimer[i] ~= nil and (tick - playerTimer[i]) > missTime then
					playerTimer[i] = nil
					if printChat == true then
						PrintChat(string.format("<font color='#FF4500'> >> miss %s</font>", object.charName))
					end
					if sendChat == true and missRange > player:GetDistanceTo(object) and (tick-lastMissTick) > missTime and player.dead == false then
						local name = object.alias
						if object.lane == LANE_TOP then
							name = "top"
						else if object.lane == LANE_MID then
							name = "mid"
						else if object.lane == LANE_BOT then
							name = "bot"
						end end end
						SendChat(string.format("ss %s",name))
						lastMissTick = tick	
					end
				end
			end
		end
	end
end

function Hotkey( msg, keycode )
	if msg == KEY_DOWN then
	   return
	end
	if sendChatKey ~= nil and keycode == sendChatKey then
		if sendChat == false then
			sendChat = true
			PrintChat("<font color='#7CFC00'> >>Automiss Call enabled!</font>")
		else
			PrintChat("<font color='#FF4500'> >>Automiss Call disabled!</font>")
			sendChat = false
		end
	end
	if printChatKey ~= nil and keycode == printChatKey then
		if printChat == false then
			printChat = true
			PrintChat("<font color='#7CFC00'> >>Automiss enabled!</font>")
		else
			PrintChat("<font color='#FF4500'> >>Automiss disabled!</font>")
			printChat = false
		end
	end
end

function Load()
	script.keyCallback = "Hotkey"
	script.timerCallback = {name = "Timer", interval = 500}
    PrintChat(" >> Auto miss script loaded!")
end